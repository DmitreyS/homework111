import './App.scss';
import ListItem from './Components/ListItem/ListItem.js';
import List from './Components/List/List.js';
import { useEffect, useState } from 'react';

function App() {
  const [productList, setProductList] = useState([]);
  const [searchProcuctList, setSearchProductList] = useState([]);
  const [searchString, setSearchString] = useState([]);


  useEffect(() => {

    fetch('https://fakestoreapi.com/products/')
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        setProductList(result);
        setSearchProductList(result);
      });
  }, []);

  useEffect(() => {
    const searchProductList = productList.filter((product) => {
      return product.title.includes(searchString);
      console.log(product);
      const productTitle = String(product.title) || '';
    });
    setSearchProductList(searchProcuctList);
  }, [searchString]
 );
  return (
    <div className="App">
      <input value={searchString} onInput={(event) => searchString(event.target.value)}></input>
      <div className={'App__list'}>
        <List list={productList}></List>

      </div>
    </div>
  );
}

export default App;
