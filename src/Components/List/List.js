import './List.scss';
import ListItem from '../ListItem/ListItem.js';


function List({ list = [] }) {
    return (
        <div className={'List'}>
            {
                list.map((item) => {
                    return <ListItem id={item.id} title={item.title} price={item.price} description={item.description} category={item.category} image={item.image}></ListItem>
                })
            }

        </div>
    )
}

export default List;


//<ListItem img={list[0].img} title={[0].title} price={[0].price} ></ListItem>
