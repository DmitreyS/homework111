import './ListItem.scss';

function ListItem({ id, title, price, description, category, image }) {
    return (
        <div className={'ListItem'}>
            <div className={'ListItem__id'}>{id}</div>
            <h2 className={'ListItem__title'}>{title}</h2>
            <div className={'ListItem__price'}>{price}</div>
            <div className={'ListItem__description'}>{description}</div>
            <div className={'ListItem__category'}>{category}</div>
            <img src={image} className={'ListItem__image'} />
            <button className={'ListItem__button'}>Добавить в корзину</button>
        </div >
    )
}


export default ListItem;

